
/*
 Si scriva una funzione, denominata iniziali, che valuti quanti caratteri iniziali sono in comune tra due stringhe date. La funzione riceve due parametri, entrambi di tipo stringa, e restituisce il numero intero.
 Ad esempio:
 • se la funzione venisse chiamata come iniziali("ciao", "cielo"), dovrebbe restituire 2 in quanto i primi due caratteri sono identici.
 • se la funzione venisse chiamata come iniziali("ciao", "salve"), dovrebbe restituire 0 in quanto nessun carattere iniziale è in comune
 Si realizzi un programma che riceva dall’utente finale due parole (di al più 20 caratteri) e chiami il sottoprogramma iniziali stampando il risultato.
 */


#include <stdio.h>
#define N 20

int commonInitials(char a[], char b[]);

int main() {
    
    char a[N+1], b[N+1];
    
    printf("Insert word 1: ");
    gets(a);
    
    printf("Insert word 2: ");
    gets(b);
    
    
    printf("Common initial characters: %d", commonInitials(a, b));
    
    
    return 0;
}


int commonInitials(char a[], char b[]) {
    
    int count = 0;
    int match = 1;
    int i;
    
    for (i = 0; a[i] != '\0' && b[i] != '\0' && match; i++) {
        if (a[i] == b[i])
            count++;
        else
            match = 0;
    }
    
        return count;
}



