/*
 Si scriva un programma che consenta di gestire l’insieme degli utenti di un sistema di messaggistica istantanea.
 Ogni utente è caratterizzato da:
 a) UserName (stringa di al più 12 caratteri)
 b) Indirizzo di posta elettronica (stringa di al più 30 caratteri)
 c) Numero di collegamenti effettuati (intero), inizialmente valorizzato a 0
 d) Durata totale in minuti dei collegamenti effettuati (intero), inizialmente valorizzato a 0
 e) Stato di connessione (tipo intero con valori “online” o “offline”), inizialmente vale “offline”
 Il programma deve gestire gli utenti utilizzando una struttura dati adeguata che permetta di memorizzare un numero di utenti ignoto a priori.
 Realizzare le seguenti funzionalità in sottoprogrammi:
 • Inserimento di un nuovo utente, quindi lettura da tastiera di username e email, il resto dei
 valori sono già inizializzati come illustrato in precedenza.
 • Login di un utente già registrato: il suo stato passa da offline a online.
 • Logout di un utente già registrato: il suo stato passa da online a offline, viene incrementato il
 numero di collegamenti effettuati e viene chiesto all'utente di inserire il tempo di connessione
 da aggiungere alla durata totale.
 • Stampa della username di tutti gli utenti online in quel momento.
 • Stampa di tutti i dati dell’utente che ha la durata media più alta relativamente ai collegamenti
 effettuati.
 Infine implementare un menù di scelta che permette di utilizzare le funzioni a disposizione.
 Esempio di esecuzione:
 1. Inserisci un utente
 2. login utente
 3. logout utente
 4. stampa tutti gli utenti collegati
 5. stampa l'utente con la durata media più elevata di connessioni 6. esci
 Inserisci la tua scelta: 1
 Inserisci lo username: nicola
 Inserisci l'email: nicola.nicolini@polimi.it
 1. inserisci un utente
 2. login utente
 3. logout utente
 4. stampa tutti gli utenti collegati
 5. stampa l'utente con la durata media più elevata di connessioni 6. esci
 Inserisci la tua scelta: 2
 Inserisci la username dell'utente che si sta connettendo: nicola Utente nicola connesso
 1. inserisci un utente
 2. login utente
 3. logout utente
 4. stampa tutti gli utenti collegati
 5. stampa l'utente con la durata media più elevata di connessioni 6. esci
 Inserisci la tua scelta: 3
 Inserisci la username dell'utente che si sta disconnettendo: nicola Inserisci per quanti minuti è stato connesso: 10
 Utente nicola disconnesso, in totale ha effettuato 1 connessione per un totale di 10 minuti
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define USERNAMEMAX 12
#define EMAILMAX 30

typedef struct User {
    char username[USERNAMEMAX+1];
    char email[EMAILMAX+1];
    int nConn; // Number of connections
    int connTime; // Connection time
    int state; // 0->Offline    1->Online
    struct User *next;
}User;

User *h; // List head (decalred as global variable)


void userLogin(User *usr);
void userLogout(User *usr);
User *addNewUser(void);
void printOnlineUsers(void);
void printMostActive(void);
User *findUsername(char s[]);

int main() {
    
    h = 0;
    
//    1. Inserisci un utente
//    2. login utente
//    3. logout utente
//    4. stampa tutti gli utenti collegati
//    5. stampa l'utente con la durata media più elevata di connessioni
//      6. esci
    
    
    printf("Choose an action: \n");
    
    printf("[1] Add new user \n");
    printf("[2] Login \n");
    printf("[3] Logout \n");
    printf("[4] Print all online users \n");
    printf("[5] Print the most (averagely) active user data \n");
    printf("[6] Quit \n");
    
    int action;
    int quit = 0;
    char chosenUsrN[USERNAMEMAX+1];
    
    do {
        printf("\n\nChoose an action: \n");
        scanf("%d", &action);
        switch (action) {
            case 1:
                addNewUser();
                break;
            case 2:
                printf("Login with username:  ");
                scanf("%s", chosenUsrN);
                userLogin(findUsername(chosenUsrN));
                break;
            case 3:
                printf("Logout with username:  ");
                scanf("%s", chosenUsrN);
                userLogout(findUsername(chosenUsrN));
                break;
            case 4:
                printOnlineUsers();
                break;
            case 5:
                printMostActive();
                break;
            case 6:
                quit = 1;
                break;
            default:
                break;
        }
        
        
    } while (!quit);
    
    return 0;
}


User *addNewUser() {
    User *new;
    
    if ( (new=(User *)malloc(sizeof(User))) ) {
        
        // Insert input data
        printf("Username: ");
        scanf("%s", new->username);
        
        printf("Email: ");
        scanf("%s", new->email);
        
        new->nConn = 0;
        new->connTime = 0;
        new->state = 0;
        
        // Add data to list
        new->next = h;
        h = new;
    }
    else
        printf("ERROR!");
    
    return h;
}


void userLogin(User *usr) {
    if (usr) {
        usr->state = 1;
        printf("\nUser '%s' connected.", usr->username);
    }
    else
        printf("User NOT FOUND!");
}

void userLogout(User *usr) {
    if (usr) {
        usr->state = 0;
        usr->nConn++;
        
        printf("How long have you been online for this session? [min]: ");
        int sessionTime = 0;
        do { scanf("%d", &sessionTime); } while (sessionTime<=0);
        usr->connTime += sessionTime;
    }
    else
        printf("User NOT FOUND!");
}

void printOnlineUsers () {
    User *tmp;
    
    printf("------------------------\n-----Online users:------\n------------------------\n\n");
    for (tmp = h; tmp; tmp=tmp->next)
        if (tmp->state)
            printf("%s", tmp->username);
    printf("\n\n------------------------");

}

void printMostActive () {
    User *tmp;
    User *usrMax;
    
    for (tmp = h, usrMax = tmp; tmp; tmp=tmp->next)
        if ((float)tmp->connTime/tmp->nConn > (float)usrMax->connTime/usrMax->nConn)
            usrMax = tmp;
    
    printf("------------------------\n---Most active user:----\n------------------------\n\n");
    printf("Username:  %s\n", usrMax->username);
    printf("Email:  %s\n", usrMax->email);
    printf("Number of connections:  %d\n", usrMax->nConn);
    printf("Total connection time:  %d\n", usrMax->connTime);
    if (usrMax->state)
        printf("Current state:  Online");
    else
        printf("Current state:  Offline");
    printf("\n\n------------------------");
    
    
}

User *findUsername(char s[]) {
    User *tmp;
    
    for (tmp = h; tmp; tmp=tmp->next)
        if (!strcmp(tmp->username, s))
            return tmp;
    
    
    printf("\nUser NOT FOUND!");
    return 0;
}
