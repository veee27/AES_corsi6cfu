//
//  main.c
//  Ex02
//
//  Created by Massimo Piazza on 24/05/2018.
//  Copyright © 2018 Massimo Piazza. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NAMEMAX 40
#define PHONEMAX 20

typedef struct Contact {
    char name[NAMEMAX+1];
    char phone[PHONEMAX+1];
    
    struct Contact *next;
}Contact;


Contact *h;

void printContacts (void);
Contact *addNewContact(void);
Contact *findName(char s[]);

int main() {
    
    h = 0;
    char chosenName[NAMEMAX+1];
    Contact *tmp;
    
    
    printf("Choose an action: \n");
    
    printf("[1] Add new contact \n");
    printf("[2] Find contact \n");
    printf("[3] Print all contacts \n");
    printf("[0] Logout \n");
    
  
    
    int action;
    
    do {
        printf("\n\nChoose an action: \n");
        scanf("%d", &action);
        switch (action) {
            case 1:
                addNewContact();
                break;
            case 2:
                printf("Search for username:  ");
                scanf("%s", chosenName);
                tmp = findName(chosenName);
                if (tmp) {
                    printf("\n\n------------------------\n");
                    printf("Name:  %s\n", tmp->name);
                    printf("Phone:  %s\n", tmp->phone);
                    printf("\n------------------------\n\n");
                }
                else
                    printf("Contact NOT FOUND!");
                break;
            case 3:
                printContacts();
                break;
            default:
                break;
        }
        
    } while (action);
    
    return 0;
}


Contact *addNewContact() {
    Contact *new;
    
    char newName[NAMEMAX+1];
    
    if ( (new=(Contact *)malloc(sizeof(Contact))) ) {
        
        // Insert input data
        printf("Name: ");
        do {
        scanf("%s", newName);
        } while (findName(newName));
        strcpy(new->name, newName);
        
        printf("Phone: ");
        scanf("%s", new->phone);
        
        
        // Add data to list
        new->next = h;
        h = new;
    }
    else
        printf("ERROR!");
    
    return h;
}


Contact *findName(char s[]) {
    Contact *tmp;
    
    for (tmp = h; tmp; tmp=tmp->next)
        if (!strcmp(tmp->name, s))
            return tmp;
    
    return 0;
}


void printContacts () {
    
    Contact *tmp;

    for (tmp = h; tmp; tmp=tmp->next) {
        
        printf("\n\n------------------------\n");
        printf("Name:  %s\n", tmp->name);
        printf("Phone:  %s\n", tmp->phone);
        printf("\n------------------------\n\n");
        
    }
    
}
