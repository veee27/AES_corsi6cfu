
#include <stdio.h>

int main() {
    
    char shape;
    int w, i,j;
    
    do {
        printf("Choose a shape ('L'=line, 'T'=triangle, 'S'=square):\n");
        scanf("%c", &shape);
        
    } while (shape!='L' && shape != 'T' && shape != 'S');
    
    do {
        printf("Integer widtb of the geometric shape: ");
        scanf("%d", &w);
    } while (w <= 0);
    
    switch (shape) {
        case 'L':
            for (i=1; i<=w; i++)
                printf("*");
            break;
        case 'T':
            for (i=1; i<=w; i++) {
                for (j=1; j<=i; j++) {
                    printf("*");
                }
                    printf("\n");
            }
            break;
        case 'S':
            for (i=1; i<=w; i++) {
                for (j=1; j<=w; j++) {
                    printf("*");
                }
                printf("\n");
            }
            break;
            
        default:
            break;
    }
    
    return 0;
}

