#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void ordina(int a1[],int a2[]);

int main()
{
	int a1[] = {3,4,5,10,12,11}, a2[] = {1,2,6,14,15,18};
	ordina(a1,a2);
	return 0;
}

void ordina(int a1[],int a2[])
{
	int i,j,temp;
	int a3[12];
	
	for(i=0;i<6;i++)    //unisco gli array
	{
		a3[i] = a1[i];
		a3[i+6] = a2[i];
	}
	
	for(i=0;i<12;i++)   //ordino l'array
	{
		for(j=i+1;j<12;j++)
		{
			if(a3[i]>a3[j])
			{
				temp = a3[i];
				a3[i] = a3[j];
				a3[j] = temp;				 
			}
		}
	}
	for(i=0;i<12;i++)   // lo stampo
	{
		printf("%d ",a3[i]);
	}
	return;
}
