#include <stdio.h>
#define NOME_FILE "magazzino.txt"
#define OK 1
#define KO 0

typedef struct articolo {
        int codice;
        char *descrizione;
        int quant_effettiva;
        int quant_minima;
        struct articolo* next;
} articolo;

typedef articolo* p_articolo;

articolo* acquisisci_dati();
int inserisci_testa(articolo * *head,articolo* nuovo); 
articolo* ricerca(articolo* head,int codice);
int cancella(articolo* *head,int codice);
int riordina(articolo* head,int codice); 
void visualizza_singolo(articolo* elem);
void visualizza_magazzino(articolo* head);
void visualizza_da_riordinare(articolo* head);
int carica_da_file(articolo* *head);        
int salva_su_file(articolo* head);    
int svuota_magazzino(articolo* *head);

int main() {
    int scelta,esito,codice;
    articolo* testa = NULL,nuovo,elem;
    
    do {
        system("CLS");
        printf("\n1. Inserisci articolo");
        printf("\n2. Ricerca articolo da codice");   
        printf("\n3. Cancella articolo da codice");  
        printf("\n4. Trova articoli da riordinare");
        printf("\n5. Riordina articolo da codice");
        printf("\n6. Visualizza magazzino");
        printf("\n7. Carica magazzino da file");
        printf("\n8. Salva magazzino su file");
        printf("\n9. Svuota magazzino");
        printf("\n10. Esci");
        printf("\nInserisci la scelta: ");
        scanf("%d",&scelta);
        
        switch (scelta) {
            case 1: nuovo = acquisisci_dati();
                    esito = inserisci_testa(&testa,nuovo);       
                    if (esito==OK) printf("\nArticolo inserito");
                    else printf("\nArticolo NON inserito");
                    break;
            
            case 2: printf("\nInserisci il codice dell'articolo da ricercare: ");
                    scanf("%d",&codice);
                    elem = ricerca(testa,codice);
                    if (elem) printf("\nCodice=%d,Descrizione=%s,Giacenza=%d,QMin=%d",
                              elem->codice,elem->descrizione,elem->quant_effettiva,
                              elem->quant_minima);  
                    else printf("\nelemento non trovato");
                    break;
            
            case 3: printf("\nInserisci il codice dell'articolo da cancellare: ");
                    scanf("%d",&codice);
                    esito = cancella(&testa,codice);                    
                    if (esito==OK) printf("\nArticolo cancellato");
                    else printf("\nArticolo NON cancellato");
                    break;
                    
            case 4: visualizza_da_riordinare(testa);
                    break;
                    
            case 5: printf("\nInserisci il codice dell'articolo da riordinare: ");
                    scanf("%d",&codice);
                    esito = riordina(testa,codice);                    
                    if (esito==OK) printf("\nArticolo riordinato");
                    else printf("\nArticolo NON riordinato");
                    break;  
                    
            case 6: visualizza_magazzino(testa);
                    break;                
                    
            case 7: esito = carica_da_file(&testa);  
                    if (esito==OK) printf("\nCaricamento completato");
                    else printf("\nArticolo NON riordinato");
                    break;        
            
            case 8: esito = salva_su_file(testa);         
                    if (esito==OK) printf("\nSalvataggio completato");
                    else printf("\nArticolo NON riordinato");
                    break; 
                    
            case 9: esito = svuota_magazzino(&testa);  
                    if (esito==OK) printf("\nSvuotamento completato");
                    else printf("\nSvuotamento NON completato");
                    break;        
                    
            case 10: printf("Arrivederci");        
                    break;
                    
            default: printf("\nscelta non corretta");        
                     break;
        }
        printf("\n\n");
        system("PAUSE");
    } while (scelta!=10);
    
} 


articolo* acquisisci_dati() {
   articolo* p_temp;
   char str[20];
   p_temp = (articolo*) malloc(sizeof(articolo));
   if (p_temp) {   // if (p_temp!=NULL)
       printf("\nInserisci il codice:");
       scanf("%d",&(p_temp->codice));
       printf("\nInserisci la descrizione:");
       scanf("%s",str);
       p_temp->descrizione = (char *)malloc(sizeof(char)*strlen(str)+1);
       strcpy(p_temp->descrizione,str);   
       printf("\nInserisci la giacenza:");
       scanf("%d",&(p_temp->quant_effettiva));
       printf("\nInserisci la quantita minima:");
       scanf("%d",&(p_temp->quant_minima));        
   }
   return p_temp;  
};

int inserisci_testa(articolo* n) {       
    if (!n) {  //if (n==NULL)
       return KO;
    } else {
       n->next = head;
       head = n;
       return OK;         
    }                                           
}; 

void visualizza_magazzino(articolo* head) {
   if (!head) printf("\nNessun elemento");  
   while (head) { // while (testa!=NULL)
         printf("\nCodice=%d,Descrizione=%s,Giacenza=%d,QMin=%d",
                head->codice,head->descrizione,head->quant_effettiva,
                head->quant_minima);  
         head = head->next;       
   }  
};

articolo* ricerca(articolo* head,int codice) {  
   while (head) {
      if (head->codice==codice) return head;    
      head = head->next;        
   }           
   return NULL;   //articolo non trovato     
};

int carica_da_file(articolo** head) {
    articolo* temp;
    FILE * fp;
    char str[20];
    int controllo;
    
    //svutare la lista
    svuota_magazzino(head);
/*    while (*head) {
      temp = (*head)->next;
      free(*head);
      *head = temp;        
    }
*/    
    
    fp = fopen(NOME_FILE,"r");
    if (!fp) return KO;   
    while (!feof(fp)) {
          temp = (articolo*) malloc(sizeof(articolo));
          controllo=fscanf(fp,"%d",&(temp->codice));  //restituisce EOF se non ha letto nulla di significativo, ad esempio una riga vuota
          if (controllo==EOF) break;  //se ha letto una riga vuota, interrompo il ciclo
          fscanf(fp,"%s",str);          
          temp->descrizione = (char *) malloc (sizeof(char)*(strlen(str)+1));
          strcpy(temp->descrizione,str);
          fscanf(fp,"%d",&(temp->quant_effettiva));       
          fscanf(fp,"%d",&(temp->quant_minima));    
          inserisci_testa(head,temp);     
    }            
    fclose(fp);
    return OK;
};  

int salva_su_file(articolo* head) {
    FILE *fp;
    
    fp = fopen(NOME_FILE,"w");
    if (!fp) return KO;
    while (head) {
          fprintf(fp,"%d\n",head->codice);
          fprintf(fp,"%s\n",head->descrizione);
          fprintf(fp,"%d\n",head->quant_effettiva);
          fprintf(fp,"%d\n",head->quant_minima);   
          head = head->next;   
    }
    fclose(fp);
    return OK;
};  

void visualizza_da_riordinare(articolo* testa) {
     
     while (testa) {
           if (testa->quant_effettiva<testa->quant_minima) {
              printf("Articolo con codice %d da riordinare",testa->codice);                                                           
           }      
           testa = testa->next;
     }
};

int riordina(articolo* head,int codice) {
   articolo* elem;
   int q;
   
   elem = ricerca(head,codice);
   if (!elem) return KO;
   printf("\nInserisci la nuova quantita: "); 
   scanf("%d",&q);
   elem->quant_effettiva = elem->quant_effettiva + q;
   return OK;
};

int cancella(articolo** head,int codice) {
    articolo* precedente=NULL;
    articolo* corrente=*head;    
       
    while (corrente) {
      if (corrente->codice==codice) {
         //trovato elemento da cancellare                                           
         if (precedente) {
            //esiste un elemento che precede quello cancellato
            precedente->next = corrente->next;         
         } else {
            //l'elemento da cancellare � il primo della lista: devo aggiornare la testa
            *head=corrente->next;    
         }       
         
         //cancello elemento
         free(corrente); 
         return OK;               
      }           
      precedente = corrente;
      corrente = corrente -> next;      
    }
    return KO;
};

int svuota_magazzino(articolo** head) {
//svutare la lista
  articolo* temp;
  while (*head) {
    temp = (*head)->next;
    free(*head);
    *head = temp;        
  }
  return OK;
}













      
 
