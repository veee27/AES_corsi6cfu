#include <stdio.h>
#define N 20

int changeCar(char, char*); 

int main()
{
   char s[N+1];  
   int i;
   char newcar;

   printf("Stringa ingresso: ");
   gets(s);

   for (i=0; s[i]!='\0'; i++)
      if (changeCar(s[i], &newcar))
         s[i] = newcar;
      else
         s[i] = ' ';
      

   printf("Nuova stringa: %s\n", s);
   system("PAUSE");
}

int changeCar(char old, char *new)
{
   if ('a'<=old && old<='z') 
   {
      *new = 'A' + old - 'a';
      return (1);
   }
   return (0);      
}
