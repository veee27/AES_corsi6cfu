#include <stdio.h>
#include <string.h>

#define FILENAME "dizionario.txt"
#define SWAP_FILENAME "swap.txt"
#define MAXLENGTH 100
#define MAXCHAR 20

void scrivi(char vocabolo[],char traduzione[]){     
    int errFile1,errFile2;
    char tmp_vocabolo[MAXCHAR],tmp_traduzione[MAXCHAR];
    int cont=0,i;
    FILE *fp,*fp_swap;
    
    
    fp=fopen(FILENAME,"r");
    fp_swap=fopen(SWAP_FILENAME,"w");
    if (fp!=NULL && fp_swap!=NULL) {
        while(!feof(fp)){
            errFile1=fscanf(fp,"%s",tmp_vocabolo);
            errFile2=fscanf(fp,"%s",tmp_traduzione); 
            if (errFile1>0 && errFile2>0) {              
               fprintf(fp_swap,"%s\n",tmp_vocabolo);
               fprintf(fp_swap,"%s\n",tmp_traduzione);
            }   
        }   
        fclose(fp);
        fclose(fp_swap);
            
        fp=fopen(FILENAME,"w");
        fp_swap=fopen(SWAP_FILENAME,"r");
        if (fp!=NULL && fp_swap!=NULL) {
          while(!feof(fp_swap)){
            errFile1=fscanf(fp_swap,"%s",tmp_vocabolo);
            errFile2=fscanf(fp_swap,"%s",tmp_traduzione); 
            if (errFile1>0 && errFile2>0) { 
              fprintf(fp,"%s\n",tmp_vocabolo);
              fprintf(fp,"%s\n",tmp_traduzione);
            }              
          }
          fprintf(fp,"%s\n",vocabolo);
          fprintf(fp,"%s\n",traduzione); 
          fclose(fp);
          fclose(fp_swap);
        }  
    }                               
}

//questa funzione pu� essere abilitata in fase di test del programma per 
//visualizzare il contenuto del file ad ogni modifica               
void stampa(){
     FILE *fp;
     int errFile1,errFile2;
     char tmp_vocabolo[MAXCHAR],tmp_traduzione[MAXCHAR];
     fp=fopen(FILENAME,"r");
     printf("\nI valori presenti nel dizionario sono:\n");
     if (fp!=NULL) {
        while(!feof(fp)){
            errFile1=fscanf(fp,"%s",tmp_vocabolo); 
            errFile2=fscanf(fp,"%s",tmp_traduzione);              
            printf("\nVocabolo : %s --> %s", tmp_vocabolo,tmp_traduzione);          
        }   
        fclose(fp);
     }   
}


void cerca(char vocabolo[]){     
     FILE *fp,*fp_swap;
     int errFile1,errFile2;
     char tmp_vocabolo[MAXCHAR],tmp_traduzione[MAXCHAR];
     fp=fopen(FILENAME,"r");
     if (fp!=NULL) {
        while(!feof(fp)){
            errFile1=fscanf(fp,"%s",tmp_vocabolo); 
            errFile2=fscanf(fp,"%s",tmp_traduzione); 
            if (strcmp(tmp_vocabolo,vocabolo)==0){
               printf("traduzione : %s \n",tmp_traduzione);
                system("PAUSE");                                                           
                break;
            }                                
        }   
        fclose(fp);
     }
}
        


int main() {
    int scelta;
    char vocabolo[MAXCHAR];
    char traduzione[MAXCHAR];
    
    do{
       system("CLS");       
       //stampa(); questa funzione pu� essere abilitata in fase di test del programma per visualizzare il contenuto del file ad ogni modifica               
       printf("TRADUTTORE \n");              
       printf("1. Traduci \n");              
       printf("2. Inserisci nuova traduzione \n");
       printf("3. Esci \n");              
       printf("Seleziona : ");              
       
       scanf("%d",&scelta);
       
       switch (scelta){
              
              case 1:
                  printf("inserisci il vocabolo : ");
                  scanf("%s",vocabolo);
                  cerca(vocabolo);
                  break;           
              
              case 2:
                  printf("inserisci il vocabolo : ");
                  scanf("%s",vocabolo);
                  printf("inserisci la traduzione : ");
                  scanf("%s",traduzione);
                  scrivi(vocabolo,traduzione);
                  break;
              
              default:
                  break;
       }               
     }while(scelta!=3);          
     printf("Arrivederci...");     
     system("PAUSE");
}


