#include <stdio.h>
#define DIM 7

int posizioneMassimo(int a[], int dim) {
    int i, pos;
	
    if (dim>0) 
    {
	    for (pos=0, i=1; i<dim; i++)
            if (a[i] > a[pos])
                pos = i;
	}
	else
	    pos = -1;
	return (pos);
}

int posizioneMinimo(int a[], int dim) {
    int i,pos;
    
	if (dim>0) 
	{
        for (pos=0, i=1; i<dim; i++)
            if (a[i] < a[pos]) 
			    pos = i;		
	}
	else
	    pos = -1;
	return (pos);    
}

int main() {
  int voti[DIM];  
  int pos_max,pos_min;
  int somma;   
  float media;
  int i; 

  somma=0;
  for (i=0; i<DIM; i++) 
  {   
      do
      {
        printf("\n\nInserisci il voto del giudice %d: ", (i+1));
        scanf("%d", &voti[i]);
      } while (voti[i]<0 || voti[i]>10);  
      somma = somma + voti[i];
  }
  
  pos_max = posizioneMassimo(voti, DIM);
  pos_min = posizioneMinimo(voti, DIM);
  somma = somma - voti[pos_max] - voti[pos_min];
  
  media = (float)somma / 5;

  printf("Media delle votazioni: %f\n", media);

  system("PAUSE");
}
