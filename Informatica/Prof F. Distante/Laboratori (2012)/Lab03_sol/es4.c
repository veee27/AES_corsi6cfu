#include <stdio.h>
#include <string.h>

#define N 12
#define M 30
#define ONLINE 1
#define OFFLINE 0
#define MAX 10

typedef struct utente {
 char username[N+1];
 char email[M+1];
 int ncoll;
 int durata;
 int stato;  
} tutente;


int main() 
{
 
 char nome[N+1];
 tutente persone[MAX];
 int scelta, minuti;
 int dim, i, trovato, pos;
 float max, media;

 dim = 0;
 do {
   printf("1. inserisci un utente\n");
   printf("2. login utente\n");
   printf("3. logout utente\n");
   printf("4. stampa tutti gli utenti collegati\n");
   printf("5. stampa utente con la durata media piu' elevata di connessioni\n");
   printf("6. esci\n");
   printf("Inserisci la tua scelta: \n");
   scanf("%d",&scelta);
   
   switch (scelta) {
    case 1: 
	    if (dim < MAX) 
		{
           printf("Inserisci lo username: \n");
	       scanf("%s", persone[dim].username);
   	       printf("Inserisci l'email \n");
	       scanf("%s", persone[dim].email);
           persone[dim].ncoll = 0;
           persone[dim].durata = 0;
           persone[dim].stato = OFFLINE;
		   dim++;
        }
		else
		   printf("Sono stati registrati %d utenti.\nImpossibile inserire nuovo utente.\n", MAX);
	    break;
    case 2: 
        printf("Inserisci il nome dell'utente che si sta connettendo: \n");
	    scanf("%s", nome);
	    for (trovato=0,i=0; i<dim && !trovato; i++)
           if (strcmp(persone[i].username, nome) == 0)
		   {
		      trovato = 1;
			  if (persone[i].stato == OFFLINE) 
			  {
				persone[i].stato = ONLINE;
				printf("Utente %s ora e' connesso.\n", nome);
			  } 
			  else
				printf("Utente %s e' gia' connesso, impossibile eseguire login.\n", nome);
		   }
		if (!trovato)
		   printf("L'utente %s non esiste.\n", nome);
	    break;
	case 3: 
        printf("Inserisci il nome dell'utente che si sta disconnettendo: \n");
	    scanf("%s", nome);
        printf("Inserisci per quanti minuti e' stato connesso: \n");
	    scanf("%d", &minuti);
		for (trovato=0,i=0; i<dim && !trovato; i++)
          if (strcmp(persone[i].username, nome) == 0)
		  {
		    trovato = 1;
			if (persone[i].stato == ONLINE) {
				persone[i].ncoll++;
				persone[i].durata = persone[i].durata + minuti;
				persone[i].stato=OFFLINE;
				printf("Utente %s ora e' disconnesso, in totale ha effettuato %d connessione per un totale di %d minuti",persone[i].username,persone[i].ncoll,persone[i].durata);
			} else {
				printf("Utente %s non e' connesso, impossibile eseguire logout.\n", nome);
			}
          }
        if (!trovato)
	      printf("L'utente %s non e' registrato.\n", nome);
        break;
    case 4:    
        for(i=0; i<dim; i++)
           if(persone[i].stato==ONLINE)
               printf("%s \n",persone[i].username);
	    break;
    case 5: 
       for(max=0,pos=-1,i=0; i<dim; i++)
	   {
          if (persone[i].ncoll>0) 
	         media = (float)persone[i].durata/persone[i].ncoll;                  
          else 
	         media=0;
          if(media > max)
		  {
             max = media;
             pos = i;
          }
       }
       if (pos >= 0) 
	      printf("%s\n", persone[pos].username);
	    break;   
     }      
     system("pause");
 } while (scelta!=6);

}
