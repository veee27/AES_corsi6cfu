#include <stdio.h>

int main() {
    
    int n;
    FILE *fp_in,*fp_out;    
    
    if (fp_in=fopen("in.txt", "r")) 
    {
       if (fp_out=fopen("out.txt", "w")) 
       {  
           fscanf(fp_in, "%d", &n);     
           while(!feof(fp_in)) {            
               if (n%2==0)
                  fprintf(fp_out, "par ");                      
               else
                  fprintf(fp_out, "dis ");      
               fscanf(fp_in, "%d", &n);                         
          }   
          fclose(fp_out);
       } 
       else
          printf("Errore nell'aprire il file out.txt\n");
       fclose(fp_in);
    } 
    else 
       printf("Errore nell'aprire il file in.txt\n");     
        
    system("PAUSE");
   
}
