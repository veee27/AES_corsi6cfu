#include <stdio.h>
#define NFILE 3
#define NPOSTI 5
#define OCCUPATO 'O'
#define LIBERO 'L'

/*
ATTENZIONE: la numerazione dei posti va da 1 a 5 e delle file da 1 a 3
Gli indici della matrice usata hanno invece valore da 0 a 4 e da 0 a 2.
Ricordarsi di togliere o aggiungere 1 quando si passa da un valore all'altro.
*/


int main(){
    char sala[NFILE][NPOSTI];
    int scelta, fila, posto;
    int i, j, cont, max, max_riga;

    //svuota sala
    for (i=0;i<NFILE;i++)
         for (j=0;j<NPOSTI;j++)
             sala[i][j] = LIBERO;
    
    do {
        printf("\n(1) Prenota un posto");
        printf("\n(2) Calcola il numero di posti liberi");
        printf("\n(3) Svuota sala");
        printf("\n(4) Individua la fila maggiormente popolata");
        printf("\n(5) Mostra la sala");
        printf("\n(6) Esci");
        printf("\nInserisci la scelta: ");
        scanf("%d", &scelta);
        
        switch (scelta) {
           case 1:   do {
                        printf("Dimmi la fila: ");                     
                        scanf("%d", &fila);
                     } while (fila<1 || fila>NFILE);
                     
					 do {
                       printf("Dimmi il posto: ");
                       scanf("%d", &posto);
                     } while (posto<1 || posto>NPOSTI);
                     
					 //prenota posto           
                     if (sala[fila-1][posto-1] == LIBERO) {
                       sala[fila-1][posto-1] = OCCUPATO;
                       printf("\nPrenotazione del posto %d nella fila %d riuscita\n", posto, fila);
                     } else 
					   printf("\nPrenotazione non riuscita: il posto � gi� occupato\n");        
                     break;   
           case 2:   cont = 0;
                     for (i=0; i<NFILE; i++)      
                         for (j=0; j<NPOSTI; j++)
                             if (sala[i][j] == LIBERO) 
							    cont++;
                     printf("\nIl numero di posti liberi e' %d\n", cont);                
                     break; 
           case 3:   for (i=0; i<NFILE; i++)
                         for (j=0; j<NPOSTI; j++)
                             sala[i][j] = LIBERO;
                     break;
           case 4:   max_riga = -1;
					 max = 0;
                     for (i=0; i<NFILE; i++)   
					 {   
                         for (cont=0,j=0; j<NPOSTI;j++)
                             if (sala[i][j] == OCCUPATO) 
							    cont++;
                         if (cont>max) 
						 {
                           max = cont;
                           max_riga = i;
                         }  
                     }                        
                     printf("\nLa riga maggiormente popolata e' %d con %d posti occupati\n", max_riga+1, max);
                     break;   
           case 5:   printf("\n***** SCHERMO ******\n");
                     for (i=0; i<NFILE; i++) 
					 {
                         for (j=0; j<NPOSTI; j++)
                             printf("%c ",sala[i][j]);
						 printf("\n");
                     }    
                     break;                 
        }       
        
    } while (scelta!=6);
	system("PAUSE");        
}

