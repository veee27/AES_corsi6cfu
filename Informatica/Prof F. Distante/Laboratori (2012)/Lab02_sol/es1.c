#include <stdio.h>
#define N 10

// npari � il numero di elementi pari contenuti nel vettore, mentre
// ndispari � il numero di elementi dispari


int main(){
     int i, npari, ndispari;
	 int v[N], p[N], d[N];
     
	 npari=0;
     ndispari=0;
     for(i=0; i<N; i++)
	 {
       printf("inserisci il %d valore:",i+1);
       scanf("%d", &v[i]);
       if(v[i]%2==0)
	   {
    	     p[npari]=v[i];
    	     npari++;
	   } 
	   else 
	   {
    	     d[ndispari]=v[i];
    	     ndispari++;
	   }
     }
      
     printf("\nNumeri PARI: \n");       
     for(i=0; i<npari; i++)
       printf("%d ", p[i]);
       
     printf("\nNumeri DISPARI: \n");     
     for(i=0; i<ndispari; i++)
       printf("%d ",d[i]);
     
     printf("\n"); 
     system("PAUSE");
}
