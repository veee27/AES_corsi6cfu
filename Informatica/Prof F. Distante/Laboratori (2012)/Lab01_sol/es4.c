#include <stdio.h>

int main()
{
  int l1, l2, l3;
 
  printf("Misura del primo lato: ");
  scanf("%d", &l1);
  printf("Misura del secondo lato: ");
  scanf("%d", &l2);
  printf("Misura del terzo lato: ");
  scanf("%d", &l3);

  if (l1 < l2+l3 && l2 < l1+l3 && l3 < l1+l2) 
  {
    if (l1 == l2 && l2 == l3)
      printf("Il triangolo e' equilatero\n");
    else if (l1 == l2 || l2 == l3)
      printf("Il triangolo e' isoscele\n");
    else
      printf("Il triangolo e' scaleno\n");
	  
    if (l1*l1 == l2*l2+l3*l3 || l2*l2 == l1*l1+l3*l3 || l3*l3 == l1*l1+l2*l2 )
      printf("Il triangolo e' rettangolo\n");
  }
  else
     printf("Le misure non corrispondono a nessun triangolo\n");

  system ("PAUSE");
}
