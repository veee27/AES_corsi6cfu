/* Dato il raggio di una circonferenza, calcola
   il valore di circonferenza ed area.

   Input: raggio
   Output: circonferenza ed area
*/

#include <stdio.h>
#define PI 3.141592

int main()
{
  float raggio, area, circonf;

  printf("Raggio della circonferenza: ");
  scanf("%f", &raggio);

  circonf = 2*PI*raggio;
  area = PI*raggio*raggio;

  printf("Circonferenza: %f\nArea: %f\n", circonf, area);
  system ("PAUSE");
}
