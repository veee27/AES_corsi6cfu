#include <stdio.h>

#define MAXLEN 255

int filecopy(char [], char []);

int main()
{
    char inFileName[MAXLEN + 1], outFileName[MAXLEN + 1];
    int totCopiati;

    printf("\nInserire il nome del file di input: ");
    gets(inFileName);

    printf("\nInserire il nome del file di output: ");
    gets(outFileName);

    totCopiati = filecopy(inFileName, outFileName);

    printf("\nSono stati copiati %d caratteri.", totCopiati);

    return 0;
}

int filecopy(char inFileName[], char outFileName[])
{
    FILE *in;
    FILE *out;
    char car;
    int letti, scritti, totCopiati;

    in=fopen(inFileName, "r");
    out=fopen(outFileName, "w");

    if (in == NULL || out == NULL)
    {
        if (in == NULL)
            printf("\nImpossibile aprire il file di input");
        else
            fclose(in);

        if (out == NULL)
            printf("\nImpossibile aprire il file di output");
        else
            fclose(out);
    }
    else
    {
        totCopiati = 0;

        do
        {
            letti = fscanf(in,"%c",&car);

            if (letti > 0)
            {
                scritti = fprintf(out,"%c",car);

                if (letti != scritti)
                    printf("\nSi � verificato un errore di scrittura!");
                else
                    totCopiati += scritti;
            }
        } while (!feof(in));

        fclose(in);
        fclose(out);
    }

    return totCopiati;
}
