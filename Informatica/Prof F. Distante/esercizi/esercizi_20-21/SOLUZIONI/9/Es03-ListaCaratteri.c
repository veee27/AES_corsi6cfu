#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAXLEN 30

typedef struct elemento
{
    char info;
    struct elemento *next;
} elemento;

int main()
{
    elemento *t, *prec, *cor, *punt;
    char s[MAXLEN + 1];
    int i;

    printf("Inserisci una stringa: ");
    gets(s);

    t = NULL;
    for (i = 0; i < strlen(s); i++)
    {
        cor = t;
        prec = NULL;
        while (cor != NULL && s[i] > cor->info)
        {
             prec = cor;
             cor = cor->next;
        }

        punt = (elemento *) malloc(sizeof(elemento));
        punt->info = s[i];
        punt->next = cor;

        if (prec != NULL)
               prec->next = punt;
        else
              t = punt;
    }

    while(t)
    {
        punt = t;
        printf("\n %c", punt->info);

        t = t->next;

        free(punt);
    }

    return 0;
}
