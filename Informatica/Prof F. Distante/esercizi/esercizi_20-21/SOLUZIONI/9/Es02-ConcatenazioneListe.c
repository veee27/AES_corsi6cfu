#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define NOMEFILE_1 "in1.txt"
#define NOMEFILE_2 "in2.txt"

typedef struct elemento
{
    int valore;
    struct elemento *next;
} elemento;

elemento *L1 = NULL;
elemento *L2 = NULL;

elemento* leggiLista(char *);
int concatenaListe();

int main()
{
    elemento *e;

    L1 = leggiLista(NOMEFILE_1);
    L2 = leggiLista(NOMEFILE_2);

    if (concatenaListe())
    {
        for (e = L1; e; e = e->next)
            printf("\n%d", e->valore);
    }
    else
        printf("\nLe due liste sono vuote!");

    return 0;
}

int concatenaListe()
{
    elemento *e;

    if (L1 == NULL && L2 == NULL)
        return 0;

    if (L1 == NULL)
        L1 = L2;
    else
    {
        for (e = L1; e->next; e = e->next);

        e->next = L2;
    }

    return 1;
}

elemento* leggiLista(char *fileName)
{
    int num;
    FILE *fp;
    elemento *nuovo, *e, *l;

    l = NULL;
    fp = fopen(fileName, "r");

    if (fp == NULL)
        printf("\nErrore durante l'apertura del file");
    else
    {
        do
        {
            if (fscanf(fp, "%d", &num) > 0)
            {
                if (nuovo = (elemento*)malloc(sizeof(elemento)))
                {
                    nuovo->next = NULL;
                    nuovo->valore = num;

                    if (l == NULL)
                        l = nuovo;
                    else
                    {
                        for (e = l; e->next; e = e->next);

                        e->next = nuovo;
                    }
                }
            }
        } while(!feof(fp));

        if (fclose(fp) == EOF)
            printf("\nErrore durante la chiusura del file");
    }

    return l;
}
