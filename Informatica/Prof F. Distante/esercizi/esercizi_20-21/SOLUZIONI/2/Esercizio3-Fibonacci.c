#include <stdio.h>
int main()
{
    int ultimo, penultimo, f, i, num;

    printf("\nInserire un numero: ");
    scanf("%d", &num);

    if (num > 0)
    {
        penultimo = 0 ;
        printf("%d\n",penultimo);
        ultimo = 1 ;
        printf("%d\n",ultimo);
        i = 2;

        while (i <= num)
        {
            f = ultimo + penultimo;
            printf("%d\n", f);
            penultimo = ultimo;
            ultimo = f;

            i = i + 1;
        }
    }
    else
        printf("\nIl numero inserito non e' corretto.");

    return 0;
}

/*
#include <stdio.h>
int main()
{
    int ultimo, penultimo, i, f, diff;
	int intMax;
    penultimo = 0 ;
    printf("%d\n",penultimo);
    ultimo = 1 ;
    printf("%d\n",ultimo);
    i = 2;
	intMax = 2147483647;
    do
    {
		diff = intMax - penultimo;

		if (diff >= ultimo)
		{
			f = ultimo + penultimo;
			printf("%d\n", f);
			penultimo = ultimo;
			ultimo = f;

			i = i + 1;
		}
    } while (i <= 100 && diff >= ultimo);

    return 0;
}



#include <stdio.h>
int main()
{
    int ultimo, penultimo, i, f;
    penultimo = 0 ;
    printf("%d\n",penultimo);
    ultimo = 1 ;
    printf("%d\n",ultimo);
    i = 2;
    do
    {
        f = ultimo + penultimo;
        if (f >= ultimo)
        {
            printf("%d\n", f);
            penultimo = ultimo;
            ultimo = f;

            i = i + 1;
        }
    } while (i <= 100 && f >= ultimo);

    return 0;
}
*/
