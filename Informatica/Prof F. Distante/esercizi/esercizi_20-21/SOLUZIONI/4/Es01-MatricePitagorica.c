#include <stdio.h>
#define N 10

int main ()
{
    int m[N][N]; // Matrice
    int n; // Numero origine degli assi
    int r, c; // Indici per righe e colonne della matrice

    do
    {
        printf("Inserire il numero che rappresenta l'origine degli assi:");
        scanf("%d",&n);

        if (n > 100 || n <=0)
            printf("Errore: il numero deve essere compreso tra 1 e 100\n");
    } while (n > 100 || n <=0);

    for (r = 0; r < N; r++)
    {
        m[r][0] = n + r;
        m[0][r] = n + r;
    }

    for (r = 0; r < N; r++)
    {
        for (c = 0; c < N; c++)
        {
            if ((r > 0) && (c > 0))
                m[r][c] = m[r][0] * m[0][c];

            printf("\t%d", m[r][c]);
        }

        printf("\n");
    }

    return 0;
}
