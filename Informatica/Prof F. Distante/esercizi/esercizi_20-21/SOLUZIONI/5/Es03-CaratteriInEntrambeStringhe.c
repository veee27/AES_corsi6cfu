#include <stdio.h>
#include <string.h>

#define MAXLEN 16

int main()
{
    char s1[MAXLEN], s2[MAXLEN]; // Stringhe
    int l1, l2; // Lunghezza stringhe
    int i, j; // Indice
    int caratterePresente; // Indica se un caratteri della prima stringa sono anche nella seconda
    int presente; // Indica se tutti i caratteri della prima stringa sono anche nella seconda

    printf("Inserire la prima stringa: ");
    gets(s1);

    printf("Inserire la seconda stringa: ");
    gets(s2);

    // Ricavo la lunghezza delle stringhe
    l1 = strlen(s1);
    l2 = strlen(s1);

    // Trasformazione delle stringhe in minuscolo
    for (i = 0; i < l1; i++)
    {
        if (s1[i] >= 'A' && s1[i] <= 'Z')
            s1[i] = s1[i] + 32;
    }

    for (i = 0; i < l2; i++)
    {
        if (s2[i] >= 'A' && s2[i] <= 'Z')
            s2[i] = s2[i] + 32;
    }

    presente = 1;
    for (i = 0; i < l1 && presente; i++)
        for (j = 0, presente=0; j < l2; j++)
            if (s1[i] == s2[j])
                presente = 1;

    if (presente)
        printf("La seconda stringa immessa contiene tutti i caratteri presenti nella prima.");
    else
        printf("La seconda stringa immessa NON contiene tutti i caratteri presenti nella prima.");

    return 0;
}
