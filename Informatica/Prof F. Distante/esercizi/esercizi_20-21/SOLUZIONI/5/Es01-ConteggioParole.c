#include <stdio.h>
#define MAXLEN 1000

int main ()
{
    char s[MAXLEN + 1];
    int i;
    int numeroParole;
    int numeroParoleC;

    printf("Inserire una stringa: ");
    gets(s);

    printf("\nLa stringa inserita �: %s\n", s);

    numeroParole = 0;
    numeroParoleC = 0;
    for (i = 0; i < MAXLEN && s[i] != '\0'; i++)
    {
        if (s[i] == ' ' || s[i] == '.')
            numeroParole++;

        if ((s[i] == 'C' || s[i] == 'c') && (i == 0 || s[i-1] == ' '))
            numeroParoleC++;
    }

    printf("Nella stringa sono presenti %d parole, %d iniziano con la lettera C", numeroParole, numeroParoleC);

    return 0;
}
