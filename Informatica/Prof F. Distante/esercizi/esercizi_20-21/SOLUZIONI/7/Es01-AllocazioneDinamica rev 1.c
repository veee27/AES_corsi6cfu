#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXLEN 2048
#define MAXLEN_P_IVA 11
#define MAXNUM_FORNITORI 10000

typedef struct fornitore
{
    char * ragioneSociale;
    char * indirizzo;
    char partitaIVA[MAXLEN_P_IVA];
} fornitore;

int main()
{
    fornitore fornitori[MAXNUM_FORNITORI], f;
    char s[MAXLEN];
    int decisione;
    int i, numFornitori, len;

    /* acquisizione prodotti */
    numFornitori = 0;
    printf("Vuoi inserire un fornitore (1: si, 0: no)?: ");
    scanf("%d", &decisione);

    while (decisione && numFornitori < MAXNUM_FORNITORI)
    {
        printf("\tFornitore n.%d:\n ", numFornitori + 1);

        printf("\tRagione Sociale: ");
        getchar();
        gets(s);

        len = strlen(s) + 1; 
        f.ragioneSociale = (char *)malloc(len * sizeof(char));
        if (f.ragioneSociale == NULL)
            printf("\nErrore durante l'allocazione della memoria.");
        else
            strcpy(f.ragioneSociale, s);

        printf("\tIndirizzo: ");
        gets(s);

        len = strlen(s) + 1;
            f.indirizzo = (char *)malloc(len * sizeof(char));
        if (f.indirizzo == NULL)
            printf("\nErrore durante l'allocazione della memoria.");
        else
            strcpy(f.indirizzo, s);

        printf("\tPartita IVA: ");
        gets(f.partitaIVA);

        fornitori[numFornitori] = f;
        numFornitori++;

        printf("\nVuoi inserire un altro fornitore (1: si, 0: no)?: ");
        scanf("%d", &decisione);
    }

    for(i=0; i<numFornitori; i++)
        printf("\n%s\t%s\t%s", fornitori[i].ragioneSociale, fornitori[i].indirizzo, fornitori[i].partitaIVA);

    return 0;
}

