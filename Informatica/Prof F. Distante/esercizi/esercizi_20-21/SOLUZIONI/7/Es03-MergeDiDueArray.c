#include <stdio.h>
#include <stdlib.h>

#define DIM1 10
#define DIM2 15

int * mergeArray(int *,int , int *, int);

int main()
{
    int a[DIM1], b[DIM2];
    int *c;
    int i;

    for (i = 0; i < DIM1; i++)
        a[i] = i * 2;

    for (i = 0; i < DIM2; i++)
        b[i] = i * 5;

    c = mergeArray(a, DIM1, b, DIM2);

    for (i = 0; i < DIM1 + DIM2; i++)
        printf("\n%d", *(c + i));

    return 0;
}

int * mergeArray(int *a, int dim1, int *b, int dim2)
{
    int *c;
    int i, j, k;

    c = (int *)malloc((dim1 + dim2) * sizeof(int));

    if (c == NULL)
        printf("\nErrore durante la creazione della matrice");
    else
    {
        i = 0;
        j = 0;
        k = 0;
        while (i < dim1 && j < dim2) {
            if(a[i] < b[j])
            {
                c[k] = a[i];
                i++;
            }
            else
            {
                c[k] = b[j];
                j++;
            }
            k++;
        }

        while(i < dim1)
        {
            c[k] = a[i];
            i++;
            k++;
        }

        while(j < dim2)
        {
            c[k] = b[j];
            j++;
            k++;
        }
    }

    return c;
}

