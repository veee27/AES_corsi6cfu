#include <stdio.h>
#include <stdlib.h>

#define ROW 2
#define COL 3

void sommamatrici(int [][COL], int [][COL], int [][COL], int, int);

int main()
{
    int a[ROW][COL], b[ROW][COL], c[ROW][COL];
    int *p;
    int i, j;

    p = a;
    printf("\nInserire i valori per la prima matrice):\n");
    for (i = 0; i < ROW; i++)
        for (j = 0; j < COL; j++)
        {
            printf("\nInserisci il valore %d,%d per la matrice A: ", i + 1, j + 1);
            scanf("%d", (p + (i * COL) + j));
        }

    printf("\nInserire i valori per la seconda matrice):\n");
    for (i = 0; i < ROW; i++)
        for (j = 0; j < COL; j++)
        {
            printf("\nInserisci il valore %d,%d per la matrice B: ", i + 1, j + 1);
            scanf("%d", &b[i][j]);
        }

    sommamatrici(a, b, c, ROW, COL);

    for (i = 0; i < ROW; i++)
    {
        printf("|");
        for (j = 0; j < COL; j++)
            printf("%d\t", a[i][j]);
        printf("|");

        printf("\t|");
        for (j = 0; j < COL; j++)
            printf("%d\t", b[i][j]);
        printf("|");

        printf("\t|");
        for (j = 0; j < COL; j++)
            printf("%d\t", c[i][j]);
        printf("|");

        printf("\n");
    }


    return 0;
}

void sommamatrici(int a[][COL], int b[][COL], int c[][COL], int row, int col)
{
    int i, j;

    for (i=0; i<row; i++)
        for (j=0; j<col; j++)
            c[i][j] = a[i][j] + b[i][j];
}
