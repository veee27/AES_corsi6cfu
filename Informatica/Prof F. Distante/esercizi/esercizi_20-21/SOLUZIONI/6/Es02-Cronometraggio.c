#include <stdio.h>

void fromMillisec(int, int *, int *, int *);

int main()
{
    int millisec, mm, sec, min;

    printf("Inserisci un tempo in millisecondi: ");
    scanf("%d", &millisec);

    fromMillisec(millisec, &mm, &sec, &min);

    printf("\nTempo: %d:%d.%d\n", min, sec, mm);

    return 0;
}

void fromMillisec(int millisec, int * mm, int * sec, int * min)
{
    //63600
    *mm = millisec % 1000; // --> 600
    *sec = millisec / 1000; // --> 63
    *min = *sec / 60; // --> 1
    *sec = *sec % 60; // --> 3
}
