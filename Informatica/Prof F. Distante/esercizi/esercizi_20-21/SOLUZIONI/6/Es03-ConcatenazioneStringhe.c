#include <stdio.h>

#define MAXLEN1 10
#define MAXLEN2 20

int strconcat(char [], int, char [], int, char [], int);

int main()
{
    char str1[MAXLEN1 + 1], str2[MAXLEN2 + 1];
    char strResult[MAXLEN1 + MAXLEN2 + 1];
    int len;

    printf("\nInserire la prima stringa:");
    gets(str1);

    printf("\nInserire la seconda stringa:");
    gets(str2);

    len = strconcat(str1, MAXLEN1, str2, MAXLEN2, strResult, MAXLEN1 + MAXLEN2 + 1);

    printf("\n\nLa stringa concatenata e': %s.\n\tLunghezza: %d", strResult, len);

    return 0;
}

int strconcat(char str1[], int str1MaxLen, char str2[], int str2MaxLen, char strResult[], int strResultMaxLen)
{
    int i, str1Len, str2Len;

    for (i = 0; str1[i] != '\0' && i < str1MaxLen && i < strResultMaxLen; i++)
    {
        strResult[i] = str1[i];
        str1Len++;
    }

    for (i = 0; str2[i] != '\0' && i < str2MaxLen && (i + str1Len) < strResultMaxLen; i++)
    {
        strResult[i + str1Len] = str2[i];
        str2Len++;
    }

    strResult[str1Len + str2Len] = '\0';
    return str1Len + str2Len;
}
