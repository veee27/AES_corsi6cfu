#include <stdio.h>

int main()
{
    int v1[10], v2[10], v3[10]; /* vettori */
    int val; /* Valore acquisito da inserire nei vettori */
    int i, j; /* Indici per i cicli */

    for (i = 1; i <= 2; i++)
    {
        for (j = 0; j < 10; j++)
        {
            printf("Vettore v%d, Numero %d: ", i, j + 1);
            scanf("%d", &val);

            if (i == 1)
            {
                v1[j] = val;

            }
            else
            {
                v2[j] = val;
            }
        }
    }

    for (j = 0; j < 10; j++)
    {
        v3[j] = v1[j] + v2[j];
        printf("Somma posizione %d: %d\n", j + 1, v3[j]);
    }

    return 0;
}
