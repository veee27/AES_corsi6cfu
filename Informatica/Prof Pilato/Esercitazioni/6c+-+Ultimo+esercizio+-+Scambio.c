#include <stdio.h>
#include <stdlib.h>
 
struct elemento {
    int val;
    struct elemento* next;
};
 
typedef struct elemento elem;
 
 
void scambio_valore(elem* a, elem* b) {
    int temp = a->val;
    a->val = b->val;
    b->val = temp;
}
 
 
void scambio_elemento(elem **head, elem* a, elem* b) {
 
    elem *before_a = *head;
    elem *before_b = *head;
 
    if(a == *head) {
        before_a = NULL;
    } else {
        while(before_a->next != a) { before_a = before_a->next; }
    }
 
    if(b == *head) {
        before_b = NULL;       
    } else {
        while(before_b->next != b) { before_b = before_b->next; }
    }
 
    if(before_a != NULL) {
        before_a->next = b;
    } else {
        *head = b;
    }
 
    if(before_b != NULL) {
        before_b->next = a;
    } else {
        *head = a;
    }
 
    elem *temp = a->next;
    a->next = b->next;
    b->next = temp;
 
}
 
void stampa(elem* head) {
 
    elem *curr = head;
    while(curr != NULL) {
        printf("%d ", curr->val);
        curr = curr->next;
    }
 
    printf("\n");
}
 
int main() {
 
    elem *e1 = malloc(sizeof(elem));        e1->val = 10;
    elem *e2 = malloc(sizeof(elem));        e2->val = 20;
    elem *e3 = malloc(sizeof(elem));        e3->val = 30;
    elem *e4 = malloc(sizeof(elem));        e4->val = 40;
    elem *e5 = malloc(sizeof(elem));        e5->val = 50;
 
    e1->next = e2;
    e2->next = e3;
    e3->next = e4;
    e4->next = e5;
    e5->next = NULL;
 
    elem *head = e1;
 
    stampa(head);
 
    scambio_valore(e2, e4);
 
    stampa(head);
 
    scambio_elemento(&head, e2, e4);
 
    stampa(head);
 
    return 0;
}

