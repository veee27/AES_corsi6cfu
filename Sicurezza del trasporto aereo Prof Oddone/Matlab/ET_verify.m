% This defines the risk matrix
f_vect = 10.^-[1 3 5 7 9];

%% DATA
P_EI = 5*1e-3
P_fail = [3*1e-3 3*1e-3 0.167168171180207 4*1e-2]'

%%

n = length(P_fail);


newEventRequested = 1;

P_vect = [];

k = 0;

G = NaN;

while k < n
    k = k+1;
            
        if k == 1
           fprintf('0->Failure\n1->Success\n2->Terminate sequence\n\n');
        end
        actionIndex = input( sprintf('Event %d type: ', k) );

        if actionIndex <= 1
           if actionIndex == 0
               P_vect = [P_vect P_fail(k)];
           else
               P_vect = [P_vect (1-P_fail(k))];
           end
 
        else
            G = input('Severity Level G = ');
            break
        end
        
end

if isnan(G)
    G = input('Severity Level G = ');
end


P = prod(P_vect)

if G~=0
    P_lim = f_vect(G);
    if (P < P_lim)
       fprintf('P=%d < P_lim=%d TRUE', P, P_lim)
    else
       fprintf('P=%d < P_lim=%d FALSE', P, P_lim)
    end
end

% P_seq = [];
% P_seq = [P_seq; P]